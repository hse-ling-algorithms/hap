package mst

import (
	"testing"
)

func TestUnit__Basic(t *testing.T) {
	graph := []Edge{Edge{1, 2, 1}, Edge{2, 3, 2}, Edge{3, 1, 3}}
	msgWeight := MST(graph)
	if msgWeight != 3 {
		t.Fatalf("Wrong sum of edge weights in the MST: expected %d, got %d", 3, msgWeight)
	}

	// Example from Introduction to Algorithms by Cormen et al, Section 23.2
	graph = []Edge{
		Edge{1, 2, 4},
		Edge{2, 3, 8},
		Edge{2, 8, 11},
		Edge{3, 4, 7},
		Edge{3, 6, 4},
		Edge{4, 5, 9},
		Edge{4, 6, 14},
		Edge{5, 6, 10},
		Edge{6, 7, 2},
		Edge{7, 8, 1},
		Edge{7, 9, 6},
		Edge{8, 9, 7},
		Edge{9, 3, 2},
	}
	msgWeight = MST(graph)
	if msgWeight != 37 {
		t.Fatalf("Wrong sum of edge weights in the MST: expected %d, got %d", 37, msgWeight)
	}
}
