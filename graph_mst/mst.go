package mst

// Edge represent a graph edge between nodes `u` and `v` with some non-negative `weight`
type Edge struct {
	u, v, weight int
}

// MST returns the sum of edges in the minimum spanning tree of the given graph
func MST(graph []Edge) int {
	return 0
}
