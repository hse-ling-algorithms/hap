# Minimum Spanning Tree

Implement an algorithm computing a [minimum spanning tree (MST)](https://en.wikipedia.org/wiki/Minimum_spanning_tree) of the given graph.

* The input graph is undirected, connected, and weighted. It is represented by an array of `Edge` structs, each contaning identifiers of two graph nodes (`u` and `v`) and the edge's weight.
* The `MST` function has to return the **sum of edges** in the MST of the given graph.
  * While there can be multiple of MSTs, sum of their edges is always the same.

```golang
// Input graph:
//
//       (10)
//    1 ----- 2
//     \     /
// (30) \   / (20)
//       \ /
//        3

graph := []Edge{Edge{1, 2, 10}, Edge{2, 3, 20}, Edge{3, 1, 30}}

MST(graph) // -> 30
```
