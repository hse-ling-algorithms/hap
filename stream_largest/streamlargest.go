package streamlargest

// Stream accepts integers and allows to get `k` largest of them in O(1)
type Stream struct {
	k int
}

// New creates a Stream instance
// func New(k int) (*Stream, error) {
// }

// Push adds an integer to the stream
// func (stream *Stream) Push(value int) {
// }

// Largest get at most `k` largest elements from the stream in arbitraty order
// func (stream *Stream) Largest() []int {
// }
