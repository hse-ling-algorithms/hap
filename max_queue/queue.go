package queue

// MaxQueue is a FIFO queue that allows fast queries for the maximum among currently enqueued elements.
type MaxQueue struct{}

// New creates an instance of MaxQueue
func New() *MaxQueue {
	return new(MaxQueue)
}

// Push inserts and element to the tail
func (q *MaxQueue) Push(value uint32) {}

// Pop removes an element from the head
func (q *MaxQueue) Pop() (uint32, error) {
	return 0, nil
}

// Max returns maximum among currently enqueued elements
func (q *MaxQueue) Max() (uint32, error) {
	return 0, nil
}
