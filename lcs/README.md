# Longest Common Subsequence

Implement an algorithm to compute the [Longest common subsequence](https://en.wikipedia.org/wiki/Longest_common_subsequence_problem) of two strings.

* Note that you are expected to find a common *subsequence*, NOT a common *substring*.
* While there may be multiple of such subsequences, it does not matter which one your algorithm picks so long as it is valid.
