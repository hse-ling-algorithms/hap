package lcs

import (
	"testing"
)

func TestUnit__Basic(t *testing.T) {
	if LCS("a", "b") != "" {
		t.Fatalf("Incorrect result")
	}
	if LCS("XMJYAUZ", "MZJAWXU") != "MJAU" {
		t.Fatalf("Incorrect result")
	}
}
