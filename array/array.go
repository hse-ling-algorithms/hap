package array

// Element is a type of an array element
type Element uint64

// Array is an implementation of list using expandable array
// with fast insertion to and deletion from the end
type Array struct {
	elems []Element
	n     int
	cap   int
}

// New creates a new Array with a given capacity
// func New(cap int) *Array {}

// Len returns the lenght of the array
// func (a *Array) Len() int {}

// Get retrieves an array element by index
// func (a *Array) Get(i int) (Element, error) {}

// Set writes an element to array by index
// func (a *Array) Set(i int, x Element) error {}

// Insert adds an element to thearray by index
// func (a *Array) Insert(i int, x Element) error {}

// Push inserts an element to the right end of the array
// func (a *Array) Push(x Element) error {}

// Delete removes an element from the array by index
// func (a *Array) Delete(i int) error {}

// Pop deletes the last element of the array
// func (a *Array) Pop() error {}

// String returns a textual representation of the array
// func (a *Array) String() string {}
