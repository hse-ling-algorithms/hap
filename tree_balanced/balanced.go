package balanced

// A Tree is a binary tree with integer values.
type Tree struct {
	Value int
	Left  *Tree
	Right *Tree
}

// New constructs a balanced BST containing elements from the given array
func New(elements []int) *Tree {

	// ...

	return nil
}
