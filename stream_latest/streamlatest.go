package streamlatest

// Stream accepts integers and allows to get any of `k` latest of them in O(1)
type Stream struct {
}

// New creates a Stream instance
func New(k int) *Stream {
	if k < 1 {
		panic("invalid argument, expected to save at least one latest element")
	}
	stream := new(Stream)

	// ...

	return stream
}

// Push adds an integer to the stream
func (s *Stream) Push(value int) {

}

// Latest returns the `i`th latest element (zero-based) and a boolean that indicates success
func (s *Stream) Latest(i int) (int, bool) {
	return -1, false
}
