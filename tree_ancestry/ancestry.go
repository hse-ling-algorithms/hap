package ancestry

// A Tree is a binary tree with integer values.
type Tree struct {
	Value int
	Left  *Tree
	Right *Tree
}

// Ancestry allows to determine if a certain tree node is a proper descendant of another in O(1)
type Ancestry struct {
}

// New creates an instance of `Ancestry` from a binary tree with unique integer nodes
func New(tree *Tree) *Ancestry {
	a := new(Ancestry)
	return a
}

// IsDescendant determines if node `b` is a proper descendant of `a` in O(1)
func (anc *Ancestry) IsDescendant(a, b int) bool {
	return false
}
