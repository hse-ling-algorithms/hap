package sym

// A Tree is a binary tree with integer values.
type Tree struct {
	Value int
	Left  *Tree
	Right *Tree
}

// IsSymTree tests whether the given tree is symmetrical
func IsSymTree(tree *Tree) bool {

	// Your code goes here

	return false
}
