# Dijkstra's algorithm

Implement an algorithm computing the shortest distance between two graph nodes using [Dijkstra's algorithm](https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm).

* The input graph is directed and edge-weighted with non-negative weights. It is represented by an array of `Edge` structs.
* The `Dijkstra` function has to return the shortest distance between the nodes `src` and `dst`.
  * Return `-1` if there is no path between `src` and `dst`.

```golang
// Input graph:
//
//       (10)
//    1 ----> 2
//     ^     /
// (5)  \   / (20)
//       \ v
//        3

graph := []Edge{Edge{1, 2, 10}, Edge{2, 3, 20}, Edge{3, 1, 5}}

Dijkstra(graph, 1, 3) // -> 30
```
