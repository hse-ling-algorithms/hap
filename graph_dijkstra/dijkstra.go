package dijkstra

// Edge represents a directed edge <u, v> with some non-negative `weight`
type Edge struct {
	u, v, weight int
}

// Dijkstra returns the shortest distance between `src` and `dst` in the given graph
func Dijkstra(graph []Edge, src, dst int) int {
	return 0
}
