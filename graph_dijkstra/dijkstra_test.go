package dijkstra

import (
	"testing"
)

func TestUnit__Basic(t *testing.T) {
	graph := []Edge{Edge{1, 2, 1}, Edge{2, 3, 2}, Edge{3, 1, 1}, Edge{1, 4, 1}}
	distance := Dijkstra(graph, 1, 3)
	if distance != 3 {
		t.Fatalf("Wrong shortest distance: expected %d, got %d", 3, distance)
	}

	if Dijkstra(graph, 4, 1) != -1 {
		t.Fatalf("-1 must be returned if there is not path between nodes")
	}

	// Example from Introduction to Algorithms by Cormen et al, Section 24.3
	graph = []Edge{
		Edge{1, 2, 10},
		Edge{1, 5, 5},
		Edge{2, 3, 1},
		Edge{2, 5, 2},
		Edge{3, 4, 4},
		Edge{4, 1, 7},
		Edge{4, 3, 6},
		Edge{5, 3, 9},
		Edge{5, 4, 2},
		Edge{5, 2, 3},
	}

	if Dijkstra(graph, 1, 3) != 9 || Dijkstra(graph, 1, 2) != 8 {
		t.Fatalf("Wrong shortest distance")
	}
}
