# Sort Linked List In Place

Implement an in-place `O(nlg(n))` algorithm for sorting a singly-linked list.

* `Sort()` gets the ‘head‘ of linked list and returns the resulting list.
* Using `sort` and `make()` is forbidden for this problem.
