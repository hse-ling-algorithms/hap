package listsort

// Node represents an element of singly-linked list
type Node struct {
	Value int
	Next  *Node
}

// Sort sorts the singly-linked list in-place
func Sort(node *Node) *Node {
	return nil
}
