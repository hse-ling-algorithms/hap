package listsort

import (
	"testing"
)

func TestUnit__Empty(t *testing.T) {
	var lst *Node
	r := Sort(lst)

	if r != nil {
		t.Fatal("`Sort` of an empty list is not empty")
	}
}

func TestUnit__Sort(t *testing.T) {

	input := []int{8, 3, 7, 1, 9, 10}
	lst := sliceToList(input)

	expected := []int{1, 3, 7, 8, 9, 10}

	r := Sort(lst)
	result := listToSlice(r)

	if len(result) != len(expected) {
		t.Fatalf("Length of the result differs from the original list")
	}

	for i := range result {
		if expected[i] != result[i] {
			t.Fatalf("Sort(%v) == %v, expected %v", input, result, expected)
		}
	}
}

func listToSlice(node *Node) []int {
	output := make([]int, 0)

	for node != nil {
		output = append(output, node.Value)
		node = node.Next
	}

	return output
}

func sliceToList(slice []int) *Node {
	var start *Node
	node := start

	for _, c := range slice {
		n := new(Node)
		n.Value = c
		if start == nil {
			start = n
			node = n
		} else {
			node.Next = n
			node = n
		}
	}
	return start
}
