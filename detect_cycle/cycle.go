package cycle

// Node represents an element of singly-linked list
type Node struct {
	Value int
	Next  *Node
}

// HasCycle detects whether the linked list has a cycle
func HasCycle(node *Node) bool {
	return false
}
